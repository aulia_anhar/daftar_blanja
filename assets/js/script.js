var baris = null

function onFormSubmit(e) {
		event.preventDefault();
	event.preventDefault();
        var formData = read();
        if (baris == null){
            insert(formData);
		}
        else{
            update(formData);
		}
        reset();    
}




//mengambil data
function read() {
    var formData = {};

    formData["nama"] = document.getElementById("nama").value;
    formData["jumlah"] = document.getElementById("jumlah").value;

    return formData;
}


//mrnyisipkan data
function insert(data) {
    var table = document.getElementById("tabel").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);

    cell1 = newRow.insertCell(0);
		cell1.innerHTML = data.nama;

    cell2 = newRow.insertCell(1);
		cell2.innerHTML = data.jumlah;

    cell3 = newRow.insertCell(2);
		cell3.innerHTML = `
        <button onClick="Edit(this)" class="btn btn-warning btn-sm edit">Edit</button>
        <button onClick="Delete(this)" class="btn btn-danger btn-sm">hapus</button>`;

    cell4 = newRow.insertCell(3);
		cell4.innerHTML = '<input type="checkbox" class="form-check-input" name="check"></input>';
}



//untuk mengdit data
function Edit(td) {
    baris = td.parentElement.parentElement;

    document.getElementById("nama").value = baris.cells[0].innerHTML;
    document.getElementById("jumlah").value = baris.cells[1].innerHTML;
}


function update(formData) {

    baris.cells[0].innerHTML = formData.nama;
    baris.cells[1].innerHTML = formData.jumlah;
}



//Untuk mengapus  data kolom
function Delete(td) {
    if (confirm('Anda ingin menghapusnya?')) {

        row = td.parentElement.parentElement;
        document.getElementById('tabel').deleteRow(row.rowIndex);
        reset();

    }
}


//Untuk tombol reset data pada kolom input
function reset() {
    document.getElementById("nama").value = '';
    document.getElementById("jumlah").value = '';
    baris = null;
}